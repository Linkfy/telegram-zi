# Telegram Zi
## Easy Telegram scenes based on buttons

### Basic Example
```javascript
require('dotenv').config();
const zi = require('./telegram-zi'); // const zi = require('telegram-zi');

(async() => {
    let bot = new zi.newBot(process.env.TELEGRAM_BOT_TOKEN);

    //Basic Scene config -> 1) scene Name, 2) scene buttons, 3) create Scene
    let exampleSceneName = "example_scene";
    zi.defineButtons(exampleSceneName, [
        ['Enter same scene again', exampleSceneName],
        ['This button does exactly the same', exampleSceneName],
    ])
    zi.newScene({sceneName: exampleSceneName, say:"Welcome @Linkfy's bot! Select an option to start"});

    zi.start(exampleSceneName);

})();

```

### Bot Auth:

```javascript
    let bot = new zi.newBot(process.env.TELEGRAM_BOT_TOKEN, ids=[5705569, 5705568]); //You can specify the authorized user ids
```

### Advanced Example:
```javascript


let startProcessScene = 'start_process';
zi.newScene({sceneName: startProcessScene, say: "Executing operation", 
                execute: {foo: buy, args: [{
                        serviceNumber: element.service, 
                        quantity: option, 
                        sceneContainingLink: sendLinkScene
                    }]}
                });

function buy(ctx, info) {
    let {serviceNumber, quantity, sceneContainingLink} = info;
    let link = ctx.session.saved[sceneContainingLink].desiredLink;
    let price = Number(quantity/100)-0.1;
    console.log(`The scene ${sceneContainingLink} has the link ${link}, and wants to use ${quantity} actions with the service number ${serviceNumber}`);
    ctx.reply(`The price for the operation is ${price} coins, [TODO]\n
    Going back to menu`);
    zi.changeScene(ctx, exampleSceneName, delay = 1000);

}

//...
```

### Advanced Example 2:
```javascript



zi.newScene({sceneName: "buy_scene", nextScene: "default_scene", 
    execute: {foo: myFunction, args: ['😃', '👍']}
});
zi.newScene({sceneName: "my_scene", say:"Send me the link please", nextScene: "BuyLikes", saveReply: "linkToBuy"});


function showMyLink(ctx, param1, param2) {
ctx.reply("It is not yet implemented but this is your reply: " + ctx.session.saved.my_scene.linkToBuy + " \nAnd also this: " + ctx.message.text + " " + param1 + " " + param2 );
//We can also change button names
zi.changeSceneButtonName("another_scene", "Buy this", "Buy again this");
//Or scenes
zi.changeScene(ctx, "welcomeScene", delay = 1000);

//...
```