const Telegraf = require('telegraf')
const session = require('telegraf/session')
const Stage = require('telegraf/stage')
const Scene = require('telegraf/scenes/base')
const Markup = require('telegraf/markup')
const { leave } = Stage
let stage = null;
let bot = null;
let sceneButtons = {};
//Create a scene manager

module.exports = {
    newBot: function(token, ids=[]) {
        bot = new Telegraf(token);
        stage = new Stage();
        
        //Auth system
        bot.use((ctx, next) => {
            if(ids.length > 0) {
                if (ids.includes(ctx.from.id)) {
                    return next()
                 } else {
                     ctx.reply("access denied for id: " + ctx.from.id);
                 }
            } else {
                return next()
            }    
        });

        bot.use(session());
        bot.use(stage.middleware());
        return bot;
    },


    newScene(info) {

        let {sceneName, say, saveReply, nextScene, execute} = info;
        const myScene = new Scene(sceneName);
        let buttons = sceneButtons[sceneName];
        
        //Emisor
        myScene.enter((ctx) => {
            //First time we need to create the saver
            if(ctx.session.saved == undefined)
                ctx.session.saved = {};

            if(say !== undefined) {
                if(buttons !== undefined){
                    ctx.reply(say,
                        Markup.inlineKeyboard(buttons).extra());
                } else {
                    ctx.reply(say);
                }
            }

            console.log("Welcome to "+ sceneName);
            if(execute !== undefined) {
                if(execute.args !== undefined) {

                    execute.foo(ctx,...execute.args);
                } else {
                    execute.foo(ctx);
                }
            } 
        }); 
        
        //Receptor
        myScene.on("message", (ctx) => {
            if(saveReply !== undefined) {
                ctx.session.saved[sceneName] = {};
                ctx.session.saved[sceneName][saveReply] = ctx.message.text;
            }
            
            switch(true) {
                case buttons !== undefined:
                    //Print again the buttons if no button is pressed on Emisor message
                    ctx.reply(say,
                        Markup.inlineKeyboard(buttons).extra()); 
                break;
                case nextScene !== undefined:
                    ctx.scene.enter(nextScene);
                    break;
                default:
                    //Stay in this scene
            }
        });
        stage.register(myScene);
        return myScene;
        
    },
    
    defineButtons(sceneName, buttons) {
        buttons.forEach((button, index) => {
            bot.action(button[1], (ctx) => ctx.scene.enter(button[1]));
            buttons[index] = [Markup.callbackButton(button[0], button[1])];
        });
        sceneButtons[sceneName] = buttons;
        
        
    },
    
    start(sceneName) {
        bot.on("message", (ctx) => ctx.scene.enter(sceneName));
        bot.startPolling();
    },

    changeSceneButtonName(sceneName, searchedName, newName) {
        
        sceneButtons[sceneName].forEach((button,index) => {
            button = button[0];
            if(button.text.toLowerCase() == searchedName.toLowerCase()) {
                sceneButtons[sceneName][index][0].text = newName;
            }
        });
        
    },

    changeScene(ctx, sceneName, delay = 100) {
        setTimeout(function() {
            ctx.scene.enter(sceneName);
        }, delay);
    }
    
}
