require('dotenv').config();
const zi = require('./telegram-zi');

(async() => {
    //let bot = new zi.newBot(process.env.TELEGRAM_BOT_TOKEN, ids=[5705569, 5705568]); //You can specify the authorized user ids to use the bot inside array
    let bot = new zi.newBot(process.env.TELEGRAM_BOT_TOKEN);

    //Basic Scene config -> 1) scene Name, 2) scene buttons, 3) create Scene
    let exampleSceneName = "example_scene";
    zi.defineButtons(exampleSceneName, [
        ['Enter same scene again', exampleSceneName],
        ['This button does exactly the same', exampleSceneName],
    ])
    zi.newScene({sceneName: exampleSceneName, say:"Welcome @Linkfy's bot! Select an option to start"});

    zi.start(exampleSceneName);

})();